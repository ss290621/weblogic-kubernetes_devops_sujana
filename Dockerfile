#Arguments (application versions)
ARG MIP_IMAGE
ARG BPS_IMAGE
ARG ESP_IMAGE
ARG DBD_IMAGE
ARG FRC_IMAGE
ARG RA_IMAGE
ARG CEPATS_IMAGE
ARG U360_IMAGE
ARG VBS_IMAGE
ARG FINDASH_IMAGE
ARG HOP_IMAGE
ARG VBA_IMAGE
ARG TIP_IMAGE
ARG IA_IMAGE
ARG ESIGN_IMAGE
ARG VBAP_IMAGE

#build zip containing domain libraries
FROM alpine:latest AS zipbuilder
MAINTAINER Sam Allen <sam.allen@tarion.com>
RUN apk --no-cache add zip
COPY wlsdeploy/ /wlsdeploy
RUN ls -la /wlsdeploy

RUN zip -r /00.model.zip /wlsdeploy

FROM acrtarion01shared.azurecr.io/oracle/middleware/weblogic:12.2.1.4-ol8-210214 AS weblogic

#install latest WDT
RUN mkdir -p /u01/wdt/models
RUN curl -L -o /u01/wdt/weblogic-deploy.zip https://github.com/oracle/weblogic-deploy-tooling/releases/latest/download/weblogic-deploy.zip && \
    cd /u01/wdt && \
    $JAVA_HOME/bin/jar xf weblogic-deploy.zip && \
    rm weblogic-deploy.zip && \
    chmod +xw weblogic-deploy/bin/*.sh && \
    chmod -R +xw weblogic-deploy/lib/python   && \
    chown -R oracle:root weblogic-deploy

RUN mkdir -p /u01/domains/app/lib && chown -R oracle:root /u01/domains/app/lib

#stage model files
COPY --from=zipbuilder /00.model.zip /u01/wdt/models
COPY model/ /u01/wdt/models
EXPOSE 7001 8001

#copy files from application builds
#should be in order of least likely to change to most likely to change for optimal image layering
FROM $MIP_IMAGE AS mip
FROM $BPS_IMAGE AS bps
FROM $ESP_IMAGE AS esp
FROM $DBD_IMAGE AS dbd
FROM $FRC_IMAGE AS frc
FROM $RA_IMAGE AS ra
FROM $CEPATS_IMAGE AS cepats
FROM $U360_IMAGE AS u360
FROM $VBS_IMAGE AS vbs
FROM $FINDASH_IMAGE AS fin
FROM $HOP_IMAGE AS hop
FROM $VBA_IMAGE AS vba
FROM $TIP_IMAGE AS tip
FROM $IA_IMAGE AS ia
FROM $ESIGN_IMAGE AS esign
FROM $VBAP_IMAGE AS vbap

FROM weblogic
ARG NAMESPACE
ARG IMAGE
COPY --from=mip /mip.zip /u01/wdt/models
COPY --from=bps /bps.zip /u01/wdt/models
COPY --from=esp /ESP.zip /u01/wdt/models
COPY --from=dbd /dashboard.zip /u01/wdt/models
COPY --from=frc /FrcConnect.zip /u01/wdt/models
COPY --from=ra /risk-assessment.zip /u01/wdt/models
COPY --from=cepats /cepats.zip /u01/wdt/models
COPY --from=u360 /u360.zip /u01/wdt/models
COPY --from=vbs /VBS.zip /u01/wdt/models
COPY --from=fin /findash.zip /u01/wdt/models
COPY --from=hop /hop.zip /u01/wdt/models
COPY --from=vba /VBApplications.zip /u01/wdt/models
COPY --from=tip /tip.zip /u01/wdt/models
COPY --from=ia /ia.zip /u01/wdt/models
COPY --from=esign /esign.zip /u01/wdt/models
COPY --from=vbap /VBApplicationsProcessing.zip /u01/wdt/models

#create operator domain yaml
WORKDIR /u01/wdt/models
RUN sed -i "s/--NAMESPACE--/${NAMESPACE}/g" base.00.yaml && sed -i "s/--IMAGE--/${IMAGE}/g" base.00.yaml
RUN MODEL_FILES=$(find -type f -name "*.yaml" | sed 's/^\|$//g'|paste -sd,) && \
 /u01/wdt/weblogic-deploy/bin/extractDomainResource.sh -oracle_home /u01/oracle/ -domain_home /u01/domains/app -model_file ${MODEL_FILES} -domain_resource_file /u01/domains/app/k8s/domain.yaml
#here-after the domain yaml can be pulled from the image.
#docker run app-domain:v17 cat /u01/domains/app/k8s/domain.yaml
# or kubectl apply -f $(docker run....)